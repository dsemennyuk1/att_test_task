import React, { Fragment } from "react";
import { SafeAreaView, StyleSheet, Text } from "react-native";
import Navigation from "./src/navigation";
import "./src/config/debugConfig";
import "./src/config/reactotronConfig";

import { Colors } from "react-native/Libraries/NewAppScreen";

const App = () => {
  return (
    <Fragment>
      <SafeAreaView style={{ flex: 1 }}>
        <Navigation />
      </SafeAreaView>
    </Fragment>
  );
};

export default App;
