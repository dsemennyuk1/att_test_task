import * as React from "react";
import { View, Image, Text } from "react-native";
import { useNavigation, RouteProp, useRoute } from "@react-navigation/native";
import { ContactType } from "src/types/ApiTypes";
import { Divider } from "react-native-paper";
import ContactCard from "src/library/components/ContactCard";

export default () => {
  const navigation = useNavigation();
  const route: RouteProp<any, any> = useRoute();
  //@ts-ignore fix typings
  const data: ContactType = route?.params;
  navigation.setOptions({title: `${data.name.first} ${data.name.last}`});

  return (
    <View style={{ flex: 1, backgroundColor: "white", padding: 16 }}>
      <View style={{flexDirection: 'row', justifyContent: 'space-around', marginVertical: 12}}>
      <Image
            style={{
              height: 120,
              width: 120,
              borderRadius: 60,
              alignSelf: "center",
            }}
            resizeMode={"contain"}
            source={{ uri: data.picture.large }}
          />

          </View>
          <Divider />
         <ContactCard title={'Bio:'}>
          <View style={{alignSelf: 'flex-start'}}>
            <Text>{data.gender}</Text>
            <Text>Address: {data.location.city}, {data.location.country}</Text>
            <Text>{data.location.street.name}, {data.location.street.number}</Text>
            </View>
          </ContactCard>
          <ContactCard title={'Contact data:'}>
            <View style={{alignSelf: 'flex-start', marginVertical: 12}}>
            <Text>Cell: {data.cell}</Text>
            <Text>Phone: {data.phone}</Text>
            <Text>Email: {data.email}</Text>
            </View>
          </ContactCard>
    </View>
  );
};
