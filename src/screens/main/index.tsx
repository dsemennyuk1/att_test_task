import * as React from "react";
import { View, FlatList } from "react-native";
import { ActivityIndicator } from "react-native-paper";
import { useApi } from "src/library/hooks/UseApi";
import Api from "src/services/Api";
import { ApiGetContactsRawType } from "src/types/ApiTypes";
import ListItem from "../../library/components/ListItem";

export default () => {
  const [page, setPage] = React.useState<number>(1);
  const data = useApi<ApiGetContactsRawType["data"]>(Api.getContacts, page);
  const flatListOnEndReached = () => {
    setPage(page + 1);
  };

  if (!data) {
    return <ActivityIndicator />;
  }

  return (
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <FlatList
        numColumns={2}
        style={{ padding: 4 }}
        onEndReached={flatListOnEndReached}
        onEndReachedThreshold={0.1}
        data={data.results}
        renderItem={({ item }) => <ListItem item={item} />}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
};
