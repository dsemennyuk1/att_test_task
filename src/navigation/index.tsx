import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import MainScreen from "src/screens/main";
import DetailedScreen from "src/screens/detailed";

const Stack = createStackNavigator();

export default () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Contacts" component={MainScreen} />
        <Stack.Screen name="Detailed" component={DetailedScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
