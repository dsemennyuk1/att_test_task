import axios, { AxiosPromise, AxiosRequestConfig } from "axios";
import { ApiGetContactsRawType } from "src/types/ApiTypes";

interface IAxios {
  get<T>(url: string, p?: AxiosRequestConfig): Promise<T>;
  post(url: string, p?: AxiosRequestConfig): AxiosPromise;
  put(url: string, p: AxiosRequestConfig): AxiosPromise;
  delete(url: string, p?: AxiosRequestConfig): AxiosPromise;
}

class Api {
  private axios: IAxios;
  constructor() {
    this.axios = axios.create({
      baseURL: "https://randomuser.me/api/", //TODO: add to env config
      timeout: 60 * 1000,
      headers: {
        "X-Auth-Token": "a144f1b35b314aa29ba51963d74b41a4",
        "Cache-Control": "no-cache",
        "Content-Type": "application/json",
      },
    });
  }

  getContacts = async (page?: number) => {
    const res = await this.get<ApiGetContactsRawType>(
      `https://randomuser.me/api/?page=${page}&results=10&seed=abc`
    );

    return res.data;
  };

  private get = async <T>(url: string, params?: Object) => {
    try {
      const response = await this.axios.get<T>(url, { params });

      return response;
    } catch (error) {
      throw Error(error.response.data);
    }
  };
}

export default new Api();
