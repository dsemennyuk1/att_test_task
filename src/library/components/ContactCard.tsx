import * as React from "react";
import { Card } from "react-native-paper";

export default ({ title, children }: { title: string; children: any }) => {
  return (
    <Card style={{ marginVertical: 12 }}>
      <Card.Title title={title} />
      <Card.Content>{children}</Card.Content>
    </Card>
  );
};
