import * as React from "react";
import { ContactType } from "src/types/ApiTypes";
import { useNavigation } from "@react-navigation/native";
import { Card, Title } from "react-native-paper";
import { View, Image, StyleSheet } from "react-native";

export default ({ item }: { item: ContactType }) => {
  const navigation = useNavigation();

  const handleNavigate = () => {
    navigation.navigate({
      name: "Detailed",
      params: item,
    });
  };

  return (
    <>
      <Card onPress={handleNavigate} style={styles.container}>
        <View>
          <Image
            style={styles.image}
            resizeMode={"contain"}
            source={{ uri: item.picture.medium }}
          />
          <Title>
            {item.name.first} {item.name.last}
          </Title>
        </View>
      </Card>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 4,
    alignItems: "center",
    justifyContent: "center",
    margin: 4,
  },
  image: {
    height: 80,
    width: 80,
    borderRadius: 40,
    alignSelf: "center",
  },
});
