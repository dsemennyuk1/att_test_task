import * as React from "react";
import { ApiGetContactsRawType } from "src/types/ApiTypes";

type useApiGeneric = <T>(
  callback: (page?: number) => any,
  page?: number
) => T | undefined;

//TODO API methods typings
//TODO unify callback
//@ts-ignore tODO refactor typings
export const useApi: useApiGeneric = (callback, page) => {
  const [data, setData] = React.useState<
    ApiGetContactsRawType["data"] | undefined
  >(undefined);

  console.log(data);
  React.useEffect(() => {
    (async () => {
      const data = await callback(page);
      setData((prevState) => {
        //VERY DIRTY there should be another way
        if (!prevState) {
          return data;
        }
        return {
          //TODO typings
          results: [...prevState.results, ...data.results],
          info: data.info,
        };
      });
    })();
  }, [page]);

  return data;
};
