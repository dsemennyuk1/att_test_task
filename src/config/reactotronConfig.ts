import Reactotron from "reactotron-react-native";

//@ts-ignore
console.log = Reactotron.log;

Reactotron.configure()
  .useReactNative()
  .connect();
