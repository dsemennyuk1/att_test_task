import { AxiosResponse } from "axios";

export type ContactType = {
  gender: string;
  name: {
    title: string;
    first: string;
    last: string;
  };
  email: string;
  location: {
    street: {
      number: number;
      name: string;
    };
    city: string;
    state: string;
    country: string;
    postcode: number;
  };
  dob: {
    age: number;
  };
  phone: string;
  cell: string;
  picture: {
    large: string;
    medium: string;
    thumbnail: string;
  };
  nat: string;
};

export type ApiGetContactsRawType = AxiosResponse<{
  results: ContactType[];
  info: {
    seed?: string;
    results?: number;
    page: number;
    varsion: string;
  };
}>;
